FROM nginx:latest

ADD src/ /var/www
ADD .docker/nginx/default.conf /etc/nginx/conf.d/default.conf
