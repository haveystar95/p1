<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbC5wMTo4MDEwXC9hcGlcL3YxXC9hdXRoXC9sb2dpbiIsImlhdCI6MTYxODk1Mjg5NCwiZXhwIjoxNjc4OTUyODk0LCJuYmYiOjE2MTg5NTI4OTQsImp0aSI6InZkTG5ZUnU4TlRoaHRsb3oiLCJzdWIiOjIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.YhKxVojKKQOrgxtWYuRLSedAzsIORZxeVmCO8nCABa4
         */
        \DB::table('customers')->insert([
            'id' => 1,
            'first_name' => 'Вася',
            'last_name' => 'Жуков',
            'middle_name' => 'Леонидович',
            'birth_date' => '23-02-1990',
            'address' => 'Днепр',
            'link_avatar' => '2/avatar3.png',
        ]);

        \DB::table('users')->insert([
            'id' => 2,
            'name' => "Жуков Вася Леонидович",
            'phone' => "093066666662",
            'type' => "customer",
            'user_type' => "App\Models\Customer",
            'user_id' => 1,
            'remember_token' => 'remember_token',
        ]);

        \DB::table('user_auth')->insert([
            'id' => 2,
            'user_id' => 2,
            'device_id' => "test_device_customer",
            'push_token' => "push_token_customer_1",
            'verification_code' => null,
            'phone_verified_at' => '2023-04-16 00:11:03',
            'fail_number_attempts' => 0,
            'ip' => null,
            'refresh_token_check' => null,
        ]);
    }
}
