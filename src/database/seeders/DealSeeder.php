<?php

namespace Database\Seeders;

use App\Models\DealRequest;
use Illuminate\Database\Seeder;

class DealSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // active deal

        \DB::table('deal_info')->insert([
            'id' => 1,
            'category_id' => 1,
            'tag_ids' => json_encode([1]),
            'comment' => 'Потрапив у ДТП',
            'price' => '124',
        ]);

        \DB::table('deal_requests')->insert([
            'id' => 1,
            'customer_id' => 2,
            'deal_info_id' => 1,
            'status' => DealRequest::STATUS_ACTIVE_REQUEST,
        ]);

        \DB::table('deal_offers')->insert([
            'id' => 1,
            'deal_request_id' => 1,
            'counselor_id' => 1,
        ]);

        \DB::table('deals')->insert([
            'id' => 1,
            'deal_info_id' => 1,
            'deal_request_id' => 1,
            'counselor_id' => 1,
            'customer_id' => 2,
            'status' => 'active',
        ]);

        //history deal

        \DB::table('deal_info')->insert([
            'id' => 2,
            'category_id' => 1,
            'tag_ids' => json_encode([1]),
            'comment' => 'Потрапив у ДТП',
            'price' => '34',
        ]);

        \DB::table('deal_requests')->insert([
            'id' => 2,
            'customer_id' => 2,
            'deal_info_id' => 2,
            'status' => DealRequest::STATUS_CANCEL_REQUEST,
        ]);

        \DB::table('deal_offers')->insert([
            'id' => 2,
            'deal_request_id' => 2,
            'counselor_id' => 1,
        ]);

        \DB::table('deals')->insert([
            'id' => 2,
            'deal_info_id' => 2,
            'deal_request_id' => 2,
            'counselor_id' => 1,
            'customer_id' => 2,
            'status' => 'history',
        ]);



        //history deal

        \DB::table('deal_info')->insert([
            'id' => 3,
            'category_id' => 1,
            'tag_ids' => json_encode([1]),
            'comment' => 'Потрапив у ДТП',
            'price' => '34',
        ]);

        \DB::table('deal_requests')->insert([
            'id' => 3,
            'customer_id' => 2,
            'deal_info_id' => 3,
            'status' => DealRequest::STATUS_CANCEL_REQUEST,
        ]);

        \DB::table('deal_offers')->insert([
            'id' => 3,
            'deal_request_id' => 3,
            'counselor_id' => 1,
        ]);

        \DB::table('deals')->insert([
            'id' => 3,
            'deal_info_id' => 3,
            'deal_request_id' => 3,
            'counselor_id' => 1,
            'customer_id' => 2,
            'status' => 'history',
        ]);
    }





}
