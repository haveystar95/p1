<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CounselorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbC5wMTo4MDEwXC9hcGlcL3YxXC9hdXRoXC9sb2dpbiIsImlhdCI6MTYxODk1Mjg5MywiZXhwIjoxNjc4OTUyODkzLCJuYmYiOjE2MTg5NTI4OTMsImp0aSI6InY0S2Y0cVFRYUd2T0luaTQiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.CKQPQzeBboUsHSy4SR4fzpvbrnSS8-WdUm0Q7OXDH8E
         */

        \DB::table('counselors')->insert([
            'id' => 1,
            'first_name' => 'Александр',
            'last_name' => 'Иванов',
            'middle_name' => 'Петрович',
            'email' => 'iv-counselor@gmail.com',
            'birth_date' => '05-07-1992',
            'work_experience' => '4',
            'work_place' => 'LawyerGroup',
            'description' => 'Юрист по вопрома ДТП',
            'link_avatar' => '1/avatar1.png',
        ]);

        \DB::table('users')->insert([
            'id' => 1,
            'name' => "Иванов Александр Петрович",
            'phone' => "09306422222",
            'type' => "counselor",
            'user_type' => "App\Models\Counselor",
            'user_id' => 1,
            'remember_token' => 'remember_token',
        ]);

        \DB::table('user_auth')->insert([
            'id' => 1,
            'user_id' => 1,
            'device_id' => "test_device_counselor",
            'push_token' => "push_token_counselor_1",
            'verification_code' => null,
            'phone_verified_at' => '2023-04-16 00:11:03',
            'fail_number_attempts' => 0,
            'ip' => null,
            'refresh_token_check' => null,
        ]);
    }
}
