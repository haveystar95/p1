<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tags')->insert([
            'name' => "Штраф",
            'category_id' => 1,
        ]);

        \DB::table('tags')->insert([
            'name' => "Дпс",
            'category_id' => 1,
        ]);
    }
}
