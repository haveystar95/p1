<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_requests', function (Blueprint $table) {

            $table->id();

            $table->bigInteger('customer_id')->unsigned();

            $table->bigInteger('deal_info_id')->unsigned();

            $table->enum('status', [
                \App\Models\DealRequest::STATUS_NEW_REQUEST,
                \App\Models\DealRequest::STATUS_ACTIVE_REQUEST,
                \App\Models\DealRequest::STATUS_CANCEL_REQUEST,
                \App\Models\DealRequest::STATUS_DONE_REQUEST
            ])->default( \App\Models\DealRequest::STATUS_NEW_REQUEST);

            $table->timestamps();

            $table->foreign('deal_info_id')
                ->references('id')->on('deal_info');

            $table->foreign('customer_id')
                ->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_requests');
    }
}
