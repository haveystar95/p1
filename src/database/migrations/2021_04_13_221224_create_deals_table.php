<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('deal_info_id')->unsigned();

            $table->bigInteger('deal_request_id')->unsigned();

            $table->bigInteger('counselor_id')->unsigned();

            $table->bigInteger('customer_id')->unsigned();

            $table->string('status')->default('active');

            $table->timestamps();

            $table->foreign('deal_info_id')
                ->references('id')->on('deal_info');

            $table->foreign('deal_request_id')
                ->references('id')->on('deal_requests');

            $table->foreign('counselor_id')
                ->references('id')->on('users');

            $table->foreign('customer_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
