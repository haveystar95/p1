<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_offers', function (Blueprint $table) {

            $table->id();

            $table->bigInteger('deal_request_id')->unsigned();

            $table->bigInteger('counselor_id')->unsigned();

            $table->timestamps();

            $table->foreign('deal_request_id')
                ->references('id')->on('deal_requests');

            $table->foreign('counselor_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_offers');
    }
}
