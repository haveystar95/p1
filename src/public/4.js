(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/modules/auth/components/ResendVerification.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/modules/auth/components/ResendVerification.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../api */ "./resources/js/modules/auth/api/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'ResendVerification',
  data: function data() {
    return {
      form: {},
      rules: {
        email: [{
          required: true,
          message: this.$t('form.rules.required', {
            'fieldName': this.$t('setting.profile.email')
          }),
          trigger: 'blur'
        }, {
          type: 'email',
          message: this.$t('form.rules.email'),
          trigger: ['blur', 'change']
        }]
      }
    };
  },
  methods: {
    resend: function resend() {
      var _this = this;

      _api__WEBPACK_IMPORTED_MODULE_0__["default"].resend(this.form).then(function (response) {
        _this.$message({
          message: response.data.message,
          type: response.data.type
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/modules/auth/components/ResendVerification.vue?vue&type=template&id=e85ccfe4&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/modules/auth/components/ResendVerification.vue?vue&type=template&id=e85ccfe4& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("h1", [_vm._v(_vm._s(_vm.$t("auth.verification.resend_title")))]),
      _vm._v(" "),
      _c(
        "el-form",
        {
          ref: "resendForm",
          attrs: {
            model: _vm.form,
            rules: _vm.rules,
            "label-position": "left",
            "label-width": "120px"
          },
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.resend($event)
            }
          }
        },
        [
          _c(
            "el-form-item",
            {
              staticClass: "form-group",
              attrs: { prop: "email", label: _vm.$t("setting.profile.email") }
            },
            [
              _c("el-input", {
                attrs: { name: "email", type: "email" },
                model: {
                  value: _vm.form.email,
                  callback: function($$v) {
                    _vm.$set(_vm.form, "email", $$v)
                  },
                  expression: "form.email"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-form-item",
            { staticStyle: { width: "100%" } },
            [
              _c(
                "el-button",
                {
                  staticClass: "w-100",
                  attrs: { type: "success" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.resend($event)
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.$t("auth.verification.resend_button")))]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/modules/auth/api/index.js":
/*!************************************************!*\
  !*** ./resources/js/modules/auth/api/index.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

var API_ENDPOINT = 'auth';
/* harmony default export */ __webpack_exports__["default"] = ({
  verify: function verify(user, query) {
    return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("".concat(API_ENDPOINT, "/email/verify/").concat(user, "?").concat(query));
  },
  resend: function resend() {
    return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post("".concat(API_ENDPOINT, "/email/resend"));
  }
});

/***/ }),

/***/ "./resources/js/modules/auth/components/ResendVerification.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/modules/auth/components/ResendVerification.vue ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ResendVerification_vue_vue_type_template_id_e85ccfe4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ResendVerification.vue?vue&type=template&id=e85ccfe4& */ "./resources/js/modules/auth/components/ResendVerification.vue?vue&type=template&id=e85ccfe4&");
/* harmony import */ var _ResendVerification_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ResendVerification.vue?vue&type=script&lang=js& */ "./resources/js/modules/auth/components/ResendVerification.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ResendVerification_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ResendVerification_vue_vue_type_template_id_e85ccfe4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ResendVerification_vue_vue_type_template_id_e85ccfe4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/modules/auth/components/ResendVerification.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/modules/auth/components/ResendVerification.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/modules/auth/components/ResendVerification.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ResendVerification_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ResendVerification.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/modules/auth/components/ResendVerification.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ResendVerification_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/modules/auth/components/ResendVerification.vue?vue&type=template&id=e85ccfe4&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/modules/auth/components/ResendVerification.vue?vue&type=template&id=e85ccfe4& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResendVerification_vue_vue_type_template_id_e85ccfe4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ResendVerification.vue?vue&type=template&id=e85ccfe4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/modules/auth/components/ResendVerification.vue?vue&type=template&id=e85ccfe4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResendVerification_vue_vue_type_template_id_e85ccfe4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResendVerification_vue_vue_type_template_id_e85ccfe4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);