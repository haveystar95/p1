(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/modules/auth/components/RegisterForm.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/modules/auth/components/RegisterForm.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _includes_Errors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../includes/Errors */ "./resources/js/includes/Errors.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'RegisterForm',
  props: {
    errors: Object,
    loading: Boolean
  },
  data: function data() {
    return {
      form: {
        email: '',
        password: '',
        password_confirmation: ''
      },
      rules: {
        email: [{
          required: true,
          message: this.$t('global.form.rules.required', {
            'fieldName': this.$t('auth.register.email_label')
          }),
          trigger: 'blur'
        }, {
          type: 'email',
          message: this.$t('global.form.rules.email'),
          trigger: 'blur'
        }],
        name: [{
          required: true,
          message: this.$t('global.form.rules.required', {
            'fieldName': this.$t('auth.register.name_label')
          }),
          trigger: 'blur'
        }, {
          trigger: 'blur',
          min: 3,
          max: 255,
          message: this.$t('global.form.rules.max', {
            'fieldName': this.$t('auth.register.name'),
            'attribute': 255
          })
        }],
        password: [{
          required: true,
          message: this.$t('global.form.rules.required', {
            'fieldName': this.$t('auth.register.password_label')
          }),
          trigger: 'blur'
        }, {
          trigger: 'blur',
          min: 8,
          max: 12,
          message: this.$t('global.form.rules.max', {
            'fieldName': this.$t('auth.register.password_label'),
            'attribute': 8
          })
        }],
        password_confirmation: [{
          required: true,
          message: this.$t('global.form.rules.required', {
            'fieldName': this.$t('auth.register.password_confirmation_label')
          })
        }, {
          validator: this.checkPassIdentical,
          trigger: 'blur'
        }]
      },
      formErrors: new _includes_Errors__WEBPACK_IMPORTED_MODULE_1__["Errors"]()
    };
  },
  watch: {
    errors: function errors(val) {
      this.formErrors.record(this.errors);
    }
  },
  methods: {
    onSubmit: function onSubmit(e) {
      var _this = this;

      this.validateForm('registerForm').then(function (valid) {
        _this.$emit('submit', _this.form);
      });
    },
    validateForm: function validateForm(formName) {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (typeof _this2.formErrors !== 'undefined') {
                  _this2.formErrors.clear(null);
                }

                return _context.abrupt("return", _this2.$refs[formName].validate());

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    checkPassIdentical: function checkPassIdentical(rule, value, callback) {
      if (!value) {
        return callback(new Error(this.$t('global.form.rules.required', {
          'fieldName': this.$t('auth.register.password_label')
        })));
      }

      if (value !== this.form.password) {
        callback(new Error(this.$t('global.form.rules.password_repeat.different')));
      } else {
        callback();
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/modules/auth/components/RegisterForm.vue?vue&type=template&id=528110c2&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/modules/auth/components/RegisterForm.vue?vue&type=template&id=528110c2& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "el-form",
    {
      ref: "registerForm",
      attrs: { model: _vm.form, rules: _vm.rules, "label-width": "130px" },
      nativeOn: {
        keyup: function($event) {
          if (
            !$event.type.indexOf("key") &&
            _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
          ) {
            return null
          }
          return _vm.onSubmit($event)
        }
      }
    },
    [
      _c(
        "el-form-item",
        {
          staticClass: "form-group",
          attrs: {
            prop: "name",
            label: _vm.$t("auth.register.name_label"),
            error: _vm.$t(_vm.formErrors.get("name"))
          }
        },
        [
          _c("el-input", {
            attrs: { name: "name", type: "test" },
            model: {
              value: _vm.form.name,
              callback: function($$v) {
                _vm.$set(_vm.form, "name", $$v)
              },
              expression: "form.name"
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-form-item",
        {
          staticClass: "form-group",
          attrs: {
            prop: "email",
            label: _vm.$t("auth.register.email_label"),
            error: _vm.$t(_vm.formErrors.get("email"))
          }
        },
        [
          _c("el-input", {
            attrs: { name: "email", type: "email" },
            model: {
              value: _vm.form.email,
              callback: function($$v) {
                _vm.$set(_vm.form, "email", $$v)
              },
              expression: "form.email"
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-form-item",
        {
          staticClass: "form-group",
          attrs: {
            prop: "password",
            label: _vm.$t("auth.register.password_label"),
            error: _vm.$t(_vm.formErrors.get("password"))
          }
        },
        [
          _c("el-input", {
            attrs: { name: "password", "show-password": "" },
            model: {
              value: _vm.form.password,
              callback: function($$v) {
                _vm.$set(_vm.form, "password", $$v)
              },
              expression: "form.password"
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-form-item",
        {
          staticClass: "form-group",
          attrs: {
            prop: "password_confirmation",
            label: _vm.$t("auth.register.password_confirmation_label")
          }
        },
        [
          _c("el-input", {
            attrs: { name: "password_confirmation", "show-password": "" },
            model: {
              value: _vm.form.password_confirmation,
              callback: function($$v) {
                _vm.$set(_vm.form, "password_confirmation", $$v)
              },
              expression: "form.password_confirmation"
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-form-item",
        { staticStyle: { width: "100%" } },
        [
          _c(
            "el-button",
            {
              staticClass: "w-100",
              attrs: { loading: _vm.loading },
              on: {
                click: function($event) {
                  $event.preventDefault()
                  return _vm.onSubmit($event)
                }
              }
            },
            [
              _vm._v(
                "\n            " +
                  _vm._s(_vm.$t("auth.register.submit_button")) +
                  "\n        "
              )
            ]
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/includes/Errors.js":
/*!*****************************************!*\
  !*** ./resources/js/includes/Errors.js ***!
  \*****************************************/
/*! exports provided: Errors */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Errors", function() { return Errors; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Errors = /*#__PURE__*/function () {
  /**
   * Create a new Errors instance.
   */
  function Errors() {
    _classCallCheck(this, Errors);

    this.errors = {};
  }
  /**
   * Determine if an errors exists for the given field.
   *
   * @param {string} field
   */


  _createClass(Errors, [{
    key: "has",
    value: function has(field) {
      return this.errors.hasOwnProperty(field);
    }
    /**
     * Determine if we have any errors.
     */

  }, {
    key: "any",
    value: function any() {
      return Object.keys(this.errors).length > 0;
    }
    /**
     * Retrieve the error message for a field.
     *
     * @param {string} field
     */

  }, {
    key: "get",
    value: function get(field) {
      if (this.errors[field]) {
        return this.errors[field][0];
      }
    }
    /**
     * Retrieve flash message if any
     *
     * @param {string} field
     */

  }, {
    key: "getFlash",
    value: function getFlash(field) {
      if (this.errors[field]) {
        return this.errors[field];
      }
    }
    /**
     * Record the new errors.
     *
     * @param {object} errors
     */

  }, {
    key: "record",
    value: function record(errors) {
      this.errors = errors;
    }
    /**
     * Clear one or all error fields.
     *
     * @param {string|null} field
     */

  }, {
    key: "clear",
    value: function clear(field) {
      if (field) {
        if (this.has(field)) {
          delete this.errors[field];
        }

        return;
      }

      this.errors = {};
    }
  }]);

  return Errors;
}();

/***/ }),

/***/ "./resources/js/modules/auth/components/RegisterForm.vue":
/*!***************************************************************!*\
  !*** ./resources/js/modules/auth/components/RegisterForm.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RegisterForm_vue_vue_type_template_id_528110c2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RegisterForm.vue?vue&type=template&id=528110c2& */ "./resources/js/modules/auth/components/RegisterForm.vue?vue&type=template&id=528110c2&");
/* harmony import */ var _RegisterForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RegisterForm.vue?vue&type=script&lang=js& */ "./resources/js/modules/auth/components/RegisterForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RegisterForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RegisterForm_vue_vue_type_template_id_528110c2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RegisterForm_vue_vue_type_template_id_528110c2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/modules/auth/components/RegisterForm.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/modules/auth/components/RegisterForm.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/modules/auth/components/RegisterForm.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RegisterForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./RegisterForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/modules/auth/components/RegisterForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RegisterForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/modules/auth/components/RegisterForm.vue?vue&type=template&id=528110c2&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/modules/auth/components/RegisterForm.vue?vue&type=template&id=528110c2& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RegisterForm_vue_vue_type_template_id_528110c2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./RegisterForm.vue?vue&type=template&id=528110c2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/modules/auth/components/RegisterForm.vue?vue&type=template&id=528110c2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RegisterForm_vue_vue_type_template_id_528110c2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RegisterForm_vue_vue_type_template_id_528110c2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);