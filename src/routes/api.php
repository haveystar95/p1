<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([

    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'v1/auth'

], function ($router) {

    Route::post('login', [\App\Http\Controllers\AuthController::class, 'login'])->name('login');
    Route::post('verify-code', [\App\Http\Controllers\AuthController::class, 'verifyCode'])->name('verifyCode');
    Route::post('logout', 'AuthController@logout')->middleware('auth:api');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me')->middleware('auth:api');
    Route::post('set-info/{role}', 'AuthController@setUserInfo')->where('role',
        \App\Models\User::CUSTOMER_ROLE . "|" . \App\Models\User::COUNSELOR_ROLE)->middleware('auth:api');

    Route::get('avatar/{filename}','AuthController@avatar');

});





Route::group([

    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'v1/deal'

], function ($router) {
    Route::post('create-request', 'DealRequestController@create')->middleware('auth:api');
    Route::get('get-request', 'DealRequestController@getActiveRequest')->middleware('auth:api');
    Route::post('change-price', 'DealRequestController@changeRequest')->middleware('auth:api');


    Route::post('list', 'DealController@getAll')->middleware('auth:api');
    Route::get('active-deal', 'DealController@getActiveDeal')->middleware('auth:api');
    Route::get('history-list', 'DealController@getHistoryDeals')->middleware('auth:api');
});


Route::group([

    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'v1/categories'

], function ($router) {
    Route::get('list', 'CategoriesController@getAll')->middleware('auth:api');
});


Route::group([
    'middleware' => 'api',

    'namespace' => 'App\Http\Controllers\Web\Auth',
    'prefix' => 'v1/auth-web'
], function ($router) {

    Route::post('login', 'AuthController@login')->name('login-web');
    Route::post('register', 'AuthController@register')->name('register-web');

    Route::group(['middleware' => 'api'], function () {
        Route::post('logout', 'AuthController@logout')->name('logout-web')->middleware('auth:api');
        Route::post('me', 'AuthController@me')->name('me-web')->middleware('auth:api');
    });

});
