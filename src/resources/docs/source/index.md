---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://local.p1:8010/docs/collection.json)

<!-- END_INFO -->

#Account management


APIs for managing users
<!-- START_2be1f0e022faf424f18f30275e61416e -->
## Create user or update token

> Example request:

```bash
curl -X POST \
    "http://local.p1:8010/api/v1/auth/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"phone":"ipsum","type":"quia","device_id":"voluptatum"}'

```

```javascript
const url = new URL(
    "http://local.p1:8010/api/v1/auth/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "phone": "ipsum",
    "type": "quia",
    "device_id": "voluptatum"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "User created successfully."
}
```
> Example response (401):

```json
{
    "message": "Validation error",
    "message_ua": "Помилка валідації.",
    "errors": {
        "type": [
            "Вибране для type значення не коректне."
        ]
    },
    "error_code": 9
}
```
> Example response (500):

```json
{
    "message": "Server Error. Check log file.",
    "message_ua": "Помилка сервера. Зверніться до адміністратора системи.",
    "error_code": 2
}
```

### HTTP Request
`POST api/v1/auth/login`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `phone` | string |  required  | 
        `type` | string |  required  | Role name(customer|counselor)
        `device_id` | string |  required  | 
    
<!-- END_2be1f0e022faf424f18f30275e61416e -->

<!-- START_17948f0ed65903ab264c22b20d3f756d -->
## Verification sms code

> Example request:

```bash
curl -X POST \
    "http://local.p1:8010/api/v1/auth/verify-code" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"phone":"+380930639563","code":"1234"}'

```

```javascript
const url = new URL(
    "http://local.p1:8010/api/v1/auth/verify-code"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "phone": "+380930639563",
    "code": "1234"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbC5wMTo4MDEwXC9hcGlcL2F1dGhcL3ZlcmlmeS1jb2RlIiwiaWF0IjoxNjEwMzk5MjY0LCJleHAiOjE2MTMwMjcyNjQsIm5iZiI6MTYxMDM5OTI2NCwianRpIjoidGdxeHlVSVRiSmh3cTVjNiIsInN1YiI6MSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.giuFcttdvig-pwdEUn7Ukdm4HAKnPw56G_oqb0TlVoM",
    "token_type": "bearer"
}
```
> Example response (401):

```json
{
    "message": "Validation error",
    "message_ua": "Помилка валідації.",
    "errors": {
        "code": [
            "Довжина цифрового поля code повинна дорівнювати 4."
        ]
    },
    "error_code": 9
}
```

### HTTP Request
`POST api/v1/auth/verify-code`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `phone` | string |  optional  | required.
        `code` | string |  optional  | required.
    
<!-- END_17948f0ed65903ab264c22b20d3f756d -->

<!-- START_a68ff660ea3d08198e527df659b17963 -->
## Log the user out (Invalidate the token).

> Example request:

```bash
curl -X POST \
    "http://local.p1:8010/api/v1/auth/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"device_id":"vel"}'

```

```javascript
const url = new URL(
    "http://local.p1:8010/api/v1/auth/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "device_id": "vel"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "User logout successfully."
}
```
> Example response (401):

```json
{
    "message": "Validation error",
    "message_ua": "Помилка валідації.",
    "errors": {
        "type": [
            "Вибране для type значення не коректне."
        ]
    },
    "error_code": 9
}
```
> Example response (500):

```json
{
    "message": "Server Error. Check log file.",
    "message_ua": "Помилка сервера. Зверніться до адміністратора системи.",
    "error_code": 2
}
```

### HTTP Request
`POST api/v1/auth/logout`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `device_id` | string |  required  | 
    
<!-- END_a68ff660ea3d08198e527df659b17963 -->

<!-- START_823c3f3bc0a262b227a58aca2dd2e5f5 -->
## Get user info by auth token

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET \
    -G "http://local.p1:8010/api/v1/auth/me" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://local.p1:8010/api/v1/auth/me"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "id": 1,
    "name": null,
    "app_id": null,
    "phone": "380930639563",
    "type": "customer",
    "created_at": "2021-01-05T12:38:02.000000Z",
    "updated_at": "2021-01-11T21:07:44.000000Z"
}
```
> Example response (403):

```json
{
    "message": "Несанкціонований вхід",
    "message_ua": "Unauthorized.",
    "error_code": 8
}
```

### HTTP Request
`GET api/v1/auth/me`


<!-- END_823c3f3bc0a262b227a58aca2dd2e5f5 -->

<!-- START_91d9f1331539ada54b14d4d44ecbfe98 -->
## Set or update user info

> Example request:

```bash
curl -X POST \
    "http://local.p1:8010/api/v1/auth/set-info/incidunt" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"first_name":"Denis","last_name":"LastName","middle_name":"LastName","birth_date":"05.07.1995"}'

```

```javascript
const url = new URL(
    "http://local.p1:8010/api/v1/auth/set-info/incidunt"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

let body = {
    "first_name": "Denis",
    "last_name": "LastName",
    "middle_name": "LastName",
    "birth_date": "05.07.1995"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "message": "User info updated successfully."
}
```
> Example response (401):

```json
{
    "message": "Validation error",
    "message_ua": "Помилка валідації.",
    "errors": {
        "code": [
            "Довжина цифрового поля code повинна дорівнювати 4."
        ]
    },
    "error_code": 9
}
```

### HTTP Request
`POST api/v1/auth/set-info/{role}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `role` |  optional  | string required Role name(customer|counselor)
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `first_name` | string. |  optional  | 
        `last_name` | string. |  optional  | 
        `middle_name` | string. |  optional  | 
        `birth_date` | string. |  optional  | 
    
<!-- END_91d9f1331539ada54b14d4d44ecbfe98 -->

#Account management for Web


APIs for managing web users
<!-- START_ab17ae73e8c2d8536d199b4071b1f6d4 -->
## Attempt to log the user into the application.

> Example request:

```bash
curl -X POST \
    "http://local.p1:8010/api/v1/auth-web/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://local.p1:8010/api/v1/auth-web/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/auth-web/login`


<!-- END_ab17ae73e8c2d8536d199b4071b1f6d4 -->

<!-- START_c5adb3f804ccee40bb8318310e0b319a -->
## api/v1/auth-web/register
> Example request:

```bash
curl -X POST \
    "http://local.p1:8010/api/v1/auth-web/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://local.p1:8010/api/v1/auth-web/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/auth-web/register`


<!-- END_c5adb3f804ccee40bb8318310e0b319a -->

<!-- START_144b21bae5c95736903487b7b6615340 -->
## Log the user out of the application.

> Example request:

```bash
curl -X POST \
    "http://local.p1:8010/api/v1/auth-web/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://local.p1:8010/api/v1/auth-web/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/auth-web/logout`


<!-- END_144b21bae5c95736903487b7b6615340 -->

<!-- START_7b556e6af5a9c0c72bf4b56093292b23 -->
## api/v1/auth-web/me
> Example request:

```bash
curl -X POST \
    "http://local.p1:8010/api/v1/auth-web/me" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```

```javascript
const url = new URL(
    "http://local.p1:8010/api/v1/auth-web/me"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v1/auth-web/me`


<!-- END_7b556e6af5a9c0c72bf4b56093292b23 -->


