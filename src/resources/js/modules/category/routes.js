import Category from "./components/Category";

export const routes = [
    {
        path: '/category',
        name: 'Category',
        component: Category,
        iconCls: 'el-icon-menu'
    },
];
