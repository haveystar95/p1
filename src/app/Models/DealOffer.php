<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DealOffer
 *
 * @property int $id
 * @property int $deal_request_id
 * @property int $counselor_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|DealOffer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DealOffer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DealOffer query()
 * @method static \Illuminate\Database\Eloquent\Builder|DealOffer whereCounselorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealOffer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealOffer whereDealRequestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealOffer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealOffer whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\DealRequest $dealRequest
 */
class DealOffer extends Model
{
    use HasFactory;

    protected $fillable = [
        'deal_request_id',
        'counselor_id',
        'comment',
        'price',
    ];

    public function dealRequest()
    {
        return $this->belongsTo(DealRequest::class);
    }
}
