<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Counselor
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $middle_name
 * @property string|null $email
 * @property string|null $birth_date
 * @property string|null $work_experience
 * @property string|null $work_place
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor query()
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor whereWorkExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor whereWorkPlace($value)
 * @mixin \Eloquent
 * @property string|null $link_avatar
 * @method static \Illuminate\Database\Eloquent\Builder|Counselor whereLinkAvatar($value)
 */
class Counselor extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'email',
        'birth_date',
        'work_experience',
        'work_place',
        'description',
    ];

    public function user()
    {
        return $this->morphOne(User::class, 'user');
    }
}
