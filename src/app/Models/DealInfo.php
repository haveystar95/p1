<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DealInfo
 *
 * @property int $id
 * @property int $category_id
 * @property mixed|null $tag_ids
 * @property string|null $comment
 * @property int|null $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Category $category
 * @property-read \App\Models\Deal|null $deal
 * @property-read \App\Models\DealOffer $dealOffer
 * @property-read \App\Models\DealRequest|null $dealRequest
 * @method static \Illuminate\Database\Eloquent\Builder|DealInfo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DealInfo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DealInfo query()
 * @method static \Illuminate\Database\Eloquent\Builder|DealInfo whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealInfo whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealInfo wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealInfo whereTagIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealInfo whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DealInfo extends Model
{
    use HasFactory;

    protected $table = 'deal_info';

    protected $fillable = [
        'category_id',
        'tag_ids',
        'comment',
        'price',
    ];

    public function deal()
    {
        return $this->hasOne(Deal::class);
    }

    public function dealRequest()
    {
        return $this->hasOne(DealRequest::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function dealOffer()
    {
        return $this->belongsTo(DealOffer::class);
    }
}
