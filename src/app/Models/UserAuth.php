<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\device_ids
 *
 * @property int $id
 * @property int $user_id
 * @property string $deviceId
 * @property string $push_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth wheredevice_id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth whereUserId($value)
 * @mixin \Eloquent
 * @property string $device_id
 * @property string|null $verification_code
 * @property string|null $phone_verified_at
 * @property int $fail_number_attempts
 * @property string|null $ip
 * @property string|null $refresh_token_check
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth whereFailNumberAttempts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth wherePhoneVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth wherePushToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth whereRefreshTokenCheck($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAuth whereVerificationCode($value)
 */
class UserAuth extends Model
{
    use HasFactory;

    protected $table = 'user_auth';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'device_id',
        'push_token',
        'verification_code',
        'phone_verified_at',
        'fail_number_attempts',
        'ip',
        'refresh_token_check',
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
