<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Logs
 *
 * @property int $id
 * @property int $type
 * @property string $data
 * @property string|null $stacktrace
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Logs newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Logs newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Logs query()
 * @method static \Illuminate\Database\Eloquent\Builder|Logs whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Logs whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Logs whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Logs whereStacktrace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Logs whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Logs whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Logs extends Model
{

    public const TYPES = [
        'fatal' => 0,
        'sms' => 1,
        'account' => 2
    ];

    protected $fillable = [
        'id',
        'type',
        'data',
        'stacktrace'
    ];

}
