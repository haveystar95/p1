<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Deal
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Deal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Deal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Deal query()
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $deal_info_id
 * @property int $deal_request_id
 * @property int $counselor_id
 * @property int $customer_id
 * @property string $status
 * @property-read \App\Models\User $counselor
 * @property-read \App\Models\User $customer
 * @property-read \App\Models\DealInfo $dealInfo
 * @property-read \App\Models\DealRequest|null $dealRequest
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereCounselorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereDealInfoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereDealRequestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereStatus($value)
 */
class Deal extends Model
{
    use HasFactory;

    public const STATUS_ACTIVE = 'active';
    public const STATUS_DONE = 'history';

    protected $fillable = [
        'id',
        'deal_info_id',
        'deal_request_id',
        'counselor_id',
        'customer_id',
        'status',
    ];


    public function dealInfo()
    {
        return $this->belongsTo(DealInfo::class);
    }

    public function dealRequest()
    {
        return $this->hasOne(DealRequest::class);
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }

    public function counselor()
    {
        return $this->belongsTo(User::class, 'counselor_id', 'id');
    }


}
