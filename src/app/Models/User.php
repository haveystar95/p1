<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $phone
 * @property string $type
 * @property string|null $verification_code
 * @property string|null $phone_verified_at
 * @property string|null $password
 * @property int $fail_number_attempts
 * @property string|null $ip
 * @property string $user_type
 * @property int $user_id
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Admin[] $admin
 * @property-read int|null $admin_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Customer[] $customer
 * @property-read int|null $customer_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Counselor[] $counselor
 * @property-read int|null $counselor_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserAuth[] $deviceIds
 * @property-read int|null $deviceIds_count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $user
 * @property-read \App\Models\UserInfo|null $userInfo
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFailNumberAttempts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereVerificationCode($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserAuth[] $auth
 * @property-read int|null $auth_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Deal[] $dealCounselor
 * @property-read int|null $deal_counselor_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Deal[] $dealCustomer
 * @property-read int|null $deal_customer_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DealOffer[] $dealOfferCounsellor
 * @property-read int|null $deal_offer_counsellor_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DealRequest[] $dealRequestCustomer
 * @property-read int|null $deal_request_customer_count
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    const CUSTOMER_ROLE = 'customer';
    const COUNSELOR_ROLE = 'counselor';
    const ADMIN_ROLE = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'app_id',
        'phone',
        'type',
        'verification_code',
        'phone_verified_at',
        'password',
        'fail_number_attempts',
        'ip',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function user()
    {
        return $this->morphTo(__FUNCTION__, 'user_type', 'user_id');
    }

    public function customer()
    {
        return $this->morphedByMany('App\Models\Customer', 'user');
    }

    public function admin()
    {
        return $this->morphedByMany('App\Models\Admin', 'user');
    }

    public function counselor()
    {
        return $this->morphedByMany('App\Models\Counselor', 'user');
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function userInfo()
    {
        return $this->hasOne(UserInfo::class);
    }

    public function auth()
    {
        return $this->hasMany(UserAuth::class);
    }

    public function dealCustomer()
    {
        return $this->hasMany(Deal::class, 'customer_id', 'id');
    }

    public function dealCounselor()
    {
        return $this->hasMany(Deal::class, 'counselor_id', 'id');
    }

    public function dealRequestCustomer()
    {
        return $this->hasMany(DealRequest::class, 'customer_id', 'id');
    }

    public function dealOfferCounsellor()
    {
        return $this->hasMany(DealOffer::class, 'counselor_id', 'id');
    }


}
