<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DealRequest
 *
 * @property int $id
 * @property int $customer_id
 * @property int $deal_info_id
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $counselor
 * @property-read \App\Models\User $customer
 * @property-read \App\Models\DealInfo $dealInfo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DealOffer[] $dealOffers
 * @property-read int|null $deal_offers_count
 * @method static \Illuminate\Database\Eloquent\Builder|DealRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DealRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DealRequest query()
 * @method static \Illuminate\Database\Eloquent\Builder|DealRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealRequest whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealRequest whereDealInfoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealRequest whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DealRequest whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DealRequest extends Model
{
    use HasFactory;

    const STATUS_NEW_REQUEST = 'new';

    const STATUS_ACTIVE_REQUEST = 'active';

    const STATUS_CANCEL_REQUEST = 'cancel';

    const STATUS_DONE_REQUEST = 'done';

    protected $fillable = [
        'deal_info_id',
        'customer_id',
        'status',
    ];

    public function dealInfo()
    {
        return $this->belongsTo(DealInfo::class);
    }

    public function dealOffers()
    {
        return $this->hasMany(DealOffer::class);
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }

    public function counselor()
    {
        return $this->belongsTo(User::class, 'counselor_id', 'id');
    }
}
