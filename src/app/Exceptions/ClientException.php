<?php


namespace App\Exceptions;


class ClientException extends \Exception
{
    public $error_code;

    public $message_ua;


    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $this->setErrorCode(ExceptionsCode::getCodeByClass(static::class));
        $this->setMessageUa(static::MESSAGE_UA);
        parent::__construct(static::MESSAGE, static::CODE, $previous);
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->error_code;
    }

    /**
     * @param mixed $error_code
     */
    public function setErrorCode($error_code)
    {
        $this->error_code = $error_code;
    }

    /**
     * @return mixed
     */
    public function getMessageUa()
    {
        return $this->message_ua;
    }

    /**
     * @param mixed $message_ua
     */
    public function setMessageUa($message_ua)
    {
        $this->message_ua = $message_ua;
    }
}
