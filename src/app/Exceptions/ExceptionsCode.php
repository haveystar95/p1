<?php

namespace App\Exceptions;

use App\Http\Services\AccountService\Exceptions\CredentialsNotValidException;
use App\Http\Services\AccountService\Exceptions\FailCreateOrUpdateUserException;
use App\Http\Services\AccountService\Exceptions\FailGetVerificationCodeException;
use App\Http\Services\AccountService\Exceptions\device_idRemoveException;
use App\Http\Services\AccountService\Exceptions\RefreshTokenException;
use App\Http\Services\AccountService\Exceptions\TypeNotMatchException;
use App\Http\Services\AccountService\Exceptions\UserNotFoundException;
use App\Http\Services\AccountService\Exceptions\VerificationCodeException;
use App\Http\Services\DealRequestService\Exceptions\ActiveDealRequestNotFoundException;
use App\Http\Services\DealRequestService\Exceptions\DealRequestAlreadyExistException;
use App\Http\Services\SmsService\Exceptions\FailSendRequestSmsException;
use App\Repositories\Exceptions\MethodNotAllowedThisRoleException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;

class ExceptionsCode
{

    public static $errors = [
        TypeNotMatchException::class => 1,
        FailSendRequestSmsException::class => 2,
        FailGetVerificationCodeException::class => 3,
        FailCreateOrUpdateUserException::class => 4,
        VerificationCodeException::class => 5,
        CredentialsNotValidException::class => 6,
        UserNotFoundException::class => 7,
        AuthenticationException::class => 8,
        ValidationException::class => 9,
        RemoveException::class => 10,
        RefreshTokenException::class => 11,
        DealRequestAlreadyExistException::class => 12,
        MethodNotAllowedThisRoleException::class => 13,
        ActiveDealRequestNotFoundException::class => 14

    ];

    public static function getCodeByClass($class)
    {
        if (key_exists($class, self::$errors)) {
            return self::$errors[$class];
        }

        foreach (self::$errors as $key => $code) {
            if (is_a($class, $key, true)) {
                return $code;
            }
        }
        return false;
    }

}

