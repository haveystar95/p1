<?php

namespace App\Exceptions;

use App\Http\Services\LogService\LogService;
use App\Models\Logs;
use Cassandra\Exception\UnauthorizedException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];


    public function register()
    {
        $this->renderable(function (ClientException $e, $request) {
            return response()->json([
                'message' => $e->getMessage(),
                'message_ua' => $e->getMessageUa(),
                'error_code' => $e->getErrorCode()
            ], $e->getCode());
        });

        $this->renderable(function (ServerException $e, $request) {
            return response()->json([
                'message' => 'Server Error. Check log file.',
                'message_ua' => 'Помилка сервера. Зверніться до адміністратора системи.',
                'error_code' => $e->getErrorCode()
            ], $e->getCode());
        });

        $this->renderable(function (AuthenticationException $e, $request) {
            return response()->json([
                'message' => 'Несанкціонований вхід',
                'message_ua' => 'Unauthorized.',
                'error_code' => ExceptionsCode::getCodeByClass(AuthenticationException::class)
            ], 403);
        });

        $this->renderable(function (ValidationException $e, $request) {
            return response()->json([
                'message' => 'Validation error',
                'message_ua' => 'Помилка валідації.',
                'errors' => $e->validator->getMessageBag()->getMessages(),
                'error_code' => ExceptionsCode::getCodeByClass(ValidationException::class)
            ], 401);
        });

        $this->renderable(function (Throwable $e, $request) {
            /** @var LogService $logService */
            $logService = app()->make(LogService::class);

            $logService->create(Logs::TYPES['fatal'], $e->getMessage(),
                $e->getTraceAsString());

            return response()->json([
                'message' => 'Server Error!.',
                'message_ua' => 'Помилка сервера. Зверніться до адміністратора системи.',
                'error_code' => 0
            ], 500);
        });


    }
}

