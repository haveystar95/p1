<?php


namespace App\Exceptions;


use App\Http\Services\LogService\LogService;

class ServerException extends \Exception
{
    public $error_code;


    public function __construct($message = '', $stacktrace = '', $code = 500)
    {
        $logService = app()->make(LogService::class);
        $this->setErrorCode(ExceptionsCode::getCodeByClass(static::class));

        $logService->create($this->getErrorCode(), $message,
            $stacktrace);

        parent::__construct($message, 500);
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->error_code;
    }

    /**
     * @param mixed $error_code
     */
    public function setErrorCode($error_code)
    {
        $this->error_code = $error_code;
    }

    /**
     * @return mixed
     */
    public function getMessageUa()
    {
        return $this->message_ua;
    }

    /**
     * @param mixed $message_ua
     */
    public function setMessageUa($message_ua)
    {
        $this->message_ua = $message_ua;
    }
}
