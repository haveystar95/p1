<?php

namespace App\Console\Commands;

use App\Http\Services\RabbitMqService\Messages\DealRequestMessage;
use App\Http\Services\RabbitMqService\Queues\DealRequestQueue;
use App\Http\Services\RabbitMqService\RabbitMqInterface;
use App\Models\DealRequest;
use Illuminate\Console\Command;

class DealRequestSender extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deal:sendRequest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        while (true) {
//            $dealRequests = DealRequest::whereStatus(DealRequest::STATUS_PENDING_REQUEST)->get();
//
//            foreach ($dealRequests as $dealRequest) {
//
//                $message = new DealRequestMessage();
//
//                $message->setCategoryId($dealRequest->customer_id);
//                $message->setComment($dealRequest->comment);
//                $message->setPrice($dealRequest->price);
//                $message->setTagIds($dealRequest->tag_ids);
//                $message->setCustomerId($dealRequest->category_id);
//
//                $dealRequest->status = DealRequest::STATUS_ACTIVE_REQUEST;
//                $dealRequest->save();
//
//                app(RabbitMqInterface::class)->publish($message, DealRequestQueue::class);
//
//            }
            usleep(5000000);
            echo "+\n";
        }


        return 0;
    }
}
