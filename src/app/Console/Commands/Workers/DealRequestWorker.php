<?php

namespace App\Console\Commands\Workers;

use App\Http\Services\RabbitMqService\Messages\DealRequestMessage;
use App\Http\Services\RabbitMqService\Queues\DealRequestQueue;
use App\Http\Services\RabbitMqService\RabbitMqInterface;
use Illuminate\Console\Command;
use PhpAmqpLib\Message\AMQPMessage;

class DealRequestWorker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Worker:DealRequest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $queueName = DealRequestQueue::NAME;


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        app(RabbitMqInterface::class)->consume($this->queueName, $this, []);
    }

    public function process(AMQPMessage $msg)
    {
        $message = DealRequestMessage::fromAmqpMessage($msg);

        print_r($message);

        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

    }
}
