<?php

namespace App\Console\Commands;

use App\Http\Services\RabbitMqService\RabbitMqInterface;
use App\Http\Services\RabbitMqService\RabbitMqService;
use Illuminate\Console\Command;

class RmqRefreshSetting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rmq:refresh-settings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update RabbitMq settings and queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->alert('Start update Rabbit MQ settings');

        $service = app(RabbitMqInterface::class);

        $service->updateSettings();

        $this->alert('Done');
        return 0;
    }
}
