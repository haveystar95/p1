<?php

namespace App\Console\Commands;

use App\Http\Services\DealService\DealServiceInterface;
use App\Http\Services\RabbitMqService\RabbitMqInterface;
use App\Http\Services\RabbitMqService\RabbitMqService;
use Illuminate\Console\Command;

class Firebase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firebase';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {


        $service = app(DealServiceInterface::class);
        $service->send();


    }
}
