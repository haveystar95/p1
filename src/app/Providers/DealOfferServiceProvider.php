<?php

namespace App\Providers;

use App\Http\Services\AccountService\AccountService;
use App\Http\Services\DealOfferService\DealOfferInterface;
use App\Http\Services\DealOfferService\DealOfferService;
use App\Http\Services\LogService\LogService;
use Illuminate\Support\ServiceProvider;

class DealOfferServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(DealOfferInterface::class, function (): DealOfferInterface {
            return new DealOfferService($this->app->make(LogService::class));
        });

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
