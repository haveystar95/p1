<?php

namespace App\Providers;

use App\Http\Services\AccountService\AccountService;
use App\Http\Services\DealOfferService\DealInterface;
use App\Http\Services\DealService\DealService;
use App\Http\Services\DealService\DealServiceInterface;
use App\Http\Services\LogService\LogService;
use Illuminate\Support\ServiceProvider;
use Kreait\Firebase\Messaging;

class DealServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(DealServiceInterface::class, function (): DealServiceInterface {
            return new DealService($this->app->make(LogService::class), $this->app->make(Messaging::class));
        });

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
