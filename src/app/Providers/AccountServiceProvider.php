<?php

namespace App\Providers;

use App\Http\Services\AccountService\AccountService;
use App\Http\Services\LogService\LogService;
use App\Http\Services\SmsService\SmsService;
use Illuminate\Support\ServiceProvider;

class AccountServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AccountService::class, function (): AccountService {
            return new AccountService($this->app->make(SmsService::class), $this->app->make(LogService::class));
        });

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
