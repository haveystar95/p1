<?php

namespace App\Providers;

use App\Http\Services\AccountService\AccountService;
use App\Http\Services\DealInfoService\DealInfoService;
use App\Http\Services\DealInfoService\DealInfoServiceInterface;
use App\Http\Services\DealRequestService\DealRequestInterface;
use App\Http\Services\DealRequestService\DealRequestService;
use App\Http\Services\LogService\LogService;
use App\Http\Services\SmsService\SmsService;
use App\Repositories\DealInfoRepositoryInterface;
use App\Repositories\DealRequestRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class DealInfoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(DealInfoServiceInterface::class, function (): DealInfoServiceInterface {
            return new DealInfoService($this->app->make(DealInfoRepositoryInterface::class));
        });

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
