<?php

namespace App\Providers;

use App\Repositories\DealInfoRepository;
use App\Repositories\DealInfoRepositoryInterface;
use App\Repositories\DealRequestRepository;
use App\Repositories\DealRequestRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            DealRequestRepositoryInterface::class,
            DealRequestRepository::class,
        );

        $this->app->bind(
            DealInfoRepositoryInterface::class,
            DealInfoRepository::class,
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
