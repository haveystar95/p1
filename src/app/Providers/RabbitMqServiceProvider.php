<?php

namespace App\Providers;

use App\Http\Services\AccountService;
use App\Http\Services\LogService\LogService;
use App\Http\Services\RabbitMqService\RabbitMqInterface;
use App\Http\Services\RabbitMqService\RabbitMqService;
use App\Http\Services\SmsService\SmsService;
use Illuminate\Support\ServiceProvider;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class RabbitMqServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(RabbitMqInterface::class, function (): RabbitMqInterface {
            $connection = new AMQPStreamConnection(env('RABBITMQ_HOST'), env('RABBITMQ_PORT'), env('RABBITMQ_USER'),
                env('RABBITMQ_PASS'));
            return new RabbitMqService($connection);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
