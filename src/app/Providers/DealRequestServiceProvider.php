<?php

namespace App\Providers;

use App\Http\Services\AccountService\AccountService;
use App\Http\Services\DealRequestService\DealRequestInterface;
use App\Http\Services\DealRequestService\DealRequestService;
use App\Http\Services\LogService\LogService;
use App\Http\Services\SmsService\SmsService;
use App\Repositories\DealRequestRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class DealRequestServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     * @return void
     */
    public function register()
    {
        $this->app->singleton(DealRequestInterface::class, function (): DealRequestInterface {
            return new DealRequestService($this->app->make(DealRequestRepositoryInterface::class),
                $this->app->make(LogService::class));
        });

    }

    /**
     * Bootstrap services.
     * @return void
     */
    public function boot()
    {
        //
    }
}
