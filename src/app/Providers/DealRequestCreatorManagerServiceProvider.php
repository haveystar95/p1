<?php
namespace App\Providers;

use App\Http\Services\ApplicationServices\DealRequestCreatorManager;
use App\Http\Services\ApplicationServices\DealRequestCreatorManagerInterface;
use App\Http\Services\DealInfoService\DealInfoServiceInterface;
use App\Http\Services\DealRequestService\DealRequestInterface;
use App\Repositories\DealRequestRepositoryInterface;
use Illuminate\Support\ServiceProvider;

final class DealRequestCreatorManagerServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(DealRequestCreatorManagerInterface::class,
            function (): DealRequestCreatorManagerInterface {
                return new DealRequestCreatorManager($this->app->make(DealRequestInterface::class),
                    $this->app->make(DealInfoServiceInterface::class));
            });
    }

    public function boot()
    {

    }
}
