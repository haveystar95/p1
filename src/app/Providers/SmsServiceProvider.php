<?php

namespace App\Providers;

use App\Http\Services\AccountService;
use App\Http\Services\LogService\LogService;
use App\Http\Services\SmsService\SmsService;
use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SmsService::class, function (): SmsService {
            return new SmsService(env('SMS_LOGIN'), env('SMS_PASSWORD'), $this->app->make(LogService::class));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
