<?php

namespace App\Repositories;

use App\Http\Services\DealRequestService\DealRequestData;
use App\Models\DealInfo;
use App\Models\DealRequest;

interface DealInfoRepositoryInterface
{
    public function findById(int $id): ?DealInfo;

    public function saveByRequestData(DealRequestData $dealRequestData): DealInfo;
}
