<?php

namespace App\Repositories;

use App\Models\DealInfo;
use App\Models\DealRequest;

interface DealRequestRepositoryInterface
{
    public function findById(int $id): ?DealRequest;

    public function getActiveRequestByCustomerId(): ?DealRequest;

    public function getActiveRequestByCounsellorId(): ?DealRequest;

    public function createByDealInfo(DealInfo $dealInfo): DealRequest;
}
