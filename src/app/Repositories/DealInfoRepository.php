<?php


namespace App\Repositories;


use App\Http\Services\DealInfoService\DealInfoServiceInterface;
use App\Http\Services\DealRequestService\DealRequestData;
use App\Models\DealInfo;
use App\Models\DealRequest;
use App\Repositories\Exceptions\MethodNotAllowedThisRoleException;
use App\Repositories\traits\CheckRoleInterface;
use App\Repositories\traits\CheckRoleTrait;

class DealInfoRepository implements DealInfoRepositoryInterface, CheckRoleInterface
{
    use CheckRoleTrait;

    public function findById(int $id): DealInfo
    {
        return DealInfo::find($id)->first();
    }

    public function saveByRequestData(DealRequestData $dealRequestData): DealInfo
    {
        $info = new DealInfo();

        $info->category_id = $dealRequestData->getCategoryId();
        $info->tag_ids = $dealRequestData->getTagIds();
        $info->comment = $dealRequestData->getComment();
        $info->price = $dealRequestData->getPrice();

        $info->save();

        return $info;
    }
}
