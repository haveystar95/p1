<?php


namespace App\Repositories\Exceptions;

use App\Exceptions\ServerException;

class MethodNotAllowedThisRoleException extends ServerException
{

}
