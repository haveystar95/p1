<?php


namespace App\Repositories;


use App\Models\DealInfo;
use App\Models\DealRequest;
use App\Repositories\Exceptions\MethodNotAllowedThisRoleException;
use App\Repositories\traits\CheckRoleInterface;
use App\Repositories\traits\CheckRoleTrait;

class DealRequestRepository implements DealRequestRepositoryInterface, CheckRoleInterface
{
    use CheckRoleTrait;

    public function findById(int $id): DealRequest
    {
        return DealRequest::find($id)->first();
    }

    public function getActiveRequestByCustomerId(): ?DealRequest
    {
        if (! $this->isCustomer()) {
            throw new MethodNotAllowedThisRoleException();
        }

        return DealRequest::where([
            'customer_id' => auth()->id(),
            'status' => DealRequest::STATUS_ACTIVE_REQUEST
        ])->first();
    }

    public function getActiveRequestByCounsellorId(): ?DealRequest
    {
        if (! $this->isCounsellor()) {
            throw new MethodNotAllowedThisRoleException();
        }

        return DealRequest::where([
            'counsellor_id' => auth()->id(),
            'status' => DealRequest::STATUS_ACTIVE_REQUEST
        ])->first();
    }

    public function createByDealInfo(DealInfo $dealInfo): DealRequest
    {
        $dealRequest = new DealRequest();
        $dealRequest->customer_id = auth()->id();
        $dealRequest->deal_info_id = $dealInfo->id;
        $dealRequest->status = DealRequest::STATUS_NEW_REQUEST;

        $dealRequest->save();

        return $dealRequest;
    }
}
