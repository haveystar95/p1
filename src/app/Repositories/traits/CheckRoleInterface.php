<?php


namespace App\Repositories\traits;


interface CheckRoleInterface
{
    public function isCustomer(): bool;

    public function isCounsellor(): bool;
}
