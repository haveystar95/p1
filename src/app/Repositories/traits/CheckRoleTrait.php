<?php


namespace App\Repositories\traits;


use App\Models\Counselor;
use App\Models\User;

trait CheckRoleTrait
{
    public function isCustomer(): bool
    {
        return auth()->user()->type === User::CUSTOMER_ROLE;
    }

    public function isCounsellor(): bool
    {
        return auth()->user()->type === User::COUNSELOR_ROLE;
    }
}
