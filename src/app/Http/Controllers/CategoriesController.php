<?php


namespace App\Http\Controllers;


use App\Http\Requests\DealHistoryRequest;
use App\Http\Requests\RequestDealRequest;
use App\Http\Services\CategoriesService\CategoriesService;
use App\Http\Services\CategoriesService\CategoriesServiceInterface;
use App\Http\Services\DealRequestService\DealRequestData;
use App\Http\Services\DealRequestService\DealRequestInterface;
use App\Http\Services\DealRequestService\DealRequestService;
use App\Http\Services\DealService\DealService;
use App\Http\Services\DealService\DealServiceInterface;
use App\Http\Services\RabbitMqService\RabbitMqService;

class CategoriesController extends Controller
{
    private CategoriesServiceInterface $service;

    public function __construct(CategoriesService $service)
    {
        $this->service = $service;
    }


    public function getAll()
    {
        return response($this->service->getAll());
    }
}
