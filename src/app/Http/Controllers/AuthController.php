<?php

namespace App\Http\Controllers;

use App\Http\Requests\LogoutRequest;
use App\Http\Requests\RefreshRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\SetUserInfoRequest;
use App\Http\Requests\VerifyCodeRequest;
use App\Http\Services\AccountService\AccountService;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;


/**
 * @group  Account management
 *
 * APIs for managing users
 */
class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */

    /** @var AccountService */
    protected $service;

    public function __construct(AccountService $service)
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'verifyCode', 'refresh', 'avatar']]);

        $this->service = $service;
    }

    /**
     * Get user info by auth token
     * @authenticated
     * @response  {
     * "id": 1,
     * "name": null,
     * "app_id": null,
     * "phone": "380930639563",
     * "type": "customer",
     * "created_at": "2021-01-05T12:38:02.000000Z",
     * "updated_at": "2021-01-11T21:07:44.000000Z"
     * }
     * @response  403 {
     * "message": "Несанкціонований вхід",
     * "message_ua": "Unauthorized.",
     * "error_code": 8
     * }
     *
     */

    public function avatar($fileName)
    {
        $path = public_path().'/storage/avatar/' . $fileName;
        return Response::download($path);
    }

    public function me()
    {
        $user = auth()->user()->user;
        return response()->json(
            auth()->user(),
        );
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @bodyParam  device_id string required
     * @response  {
     * "message": "User logout successfully."
     * }
     * @response  401 {
     * "message": "Validation error",
     * "message_ua": "Помилка валідації.",
     * "errors": {
     * "type": [
     * "Вибране для type значення не коректне."
     * ]
     * },
     * "error_code": 9
     * }
     * @response  500 {
     * "message": "Server Error. Check log file.",
     * "message_ua": "Помилка сервера. Зверніться до адміністратора системи.",
     * "error_code": 2
     * }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(LogoutRequest $request)
    {
        $this->service->removedevice_idForUser($request->get('device_id'));
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($tokens)
    {
        return response()->json([
            'access_token' => $tokens['token'],
            'refresh_token' => $tokens['refresh_token'],
            'token_type' => 'bearer',
        ]);
    }


    /**
     * Create user or update token
     * @bodyParam  phone string required
     * @bodyParam  type string required Role name(customer|counselor)
     * @bodyParam  device_id string required
     * @bodyParam  push_token string required
     * @response  {
     * "message": "User created successfully."
     * }
     * @response  401 {
     * "message": "Validation error",
     * "message_ua": "Помилка валідації.",
     * "errors": {
     * "type": [
     * "Вибране для type значення не коректне."
     * ]
     * },
     * "error_code": 9
     * }
     * @response  500 {
     * "message": "Server Error. Check log file.",
     * "message_ua": "Помилка сервера. Зверніться до адміністратора системи.",
     * "error_code": 2
     * }
     *
     */
    public function login(RegisterRequest $request)
    {
        $result = $this->service->create($request->get('phone'), $request->get('type'), $request->get('device_id'),
            $request->ip(), $request->get('push_token'));

        return response()->json([
            'message' => __('auth.success_create')
        ], 200);
    }


    /**
     * Verification sms code
     * @bodyParam  phone string required. Example: +380930639563
     * @bodyParam  code string required. Example: 1234
     * @response  {
     * "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbC5wMTo4MDEwXC9hcGlcL2F1dGhcL3ZlcmlmeS1jb2RlIiwiaWF0IjoxNjEwMzk5MjY0LCJleHAiOjE2MTMwMjcyNjQsIm5iZiI6MTYxMDM5OTI2NCwianRpIjoidGdxeHlVSVRiSmh3cTVjNiIsInN1YiI6MSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.giuFcttdvig-pwdEUn7Ukdm4HAKnPw56G_oqb0TlVoM",
     * "token_type": "bearer"
     * }
     * @response  401 {
     * "message": "Validation error",
     * "message_ua": "Помилка валідації.",
     * "errors": {
     * "code": [
     * "Довжина цифрового поля code повинна дорівнювати 4."
     * ]
     * },
     * "error_code": 9
     * }
     */
    public function verifyCode(VerifyCodeRequest $request)
    {
        $result = $this->service->verifyCode($request->get('phone'), $request->get('code'), $request->get('device_id'));
        return $this->respondWithToken($result);
    }

    /**
     * Set or update user info
     * @urlParam role string required Role name(customer|counselor)
     * @bodyParam  first_name string. Example: Denis
     * @bodyParam  last_name string. Example: LastName
     * @bodyParam  middle_name string. Example: LastName
     * @bodyParam  birth_date  string. Example: 05.07.1995
     * @response  {
     *             "message": "User info updated successfully."
     *            }
     * @response  401 {
     * "message": "Validation error",
     * "message_ua": "Помилка валідації.",
     * "errors": {
     * "code": [
     * "Довжина цифрового поля code повинна дорівнювати 4."
     * ]
     * },
     * "error_code": 9
     * }
     */
    public function setUserInfo(SetUserInfoRequest $request)
    {
        $this->service->setUserInfo($request->all());

        return response()->json([
            'message' => __('auth.success_update_info')
        ], 200);
    }

    public function refresh(Request $request)
    {
        $result = $this->service->refreshToken($request->get('refresh_token'));
        return $this->respondWithToken($result);
    }
}
