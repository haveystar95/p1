<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePriceRequest;
use App\Http\Requests\DealHistoryRequest;
use App\Http\Requests\RequestDealRequest;
use App\Http\Services\ApplicationServices\DealRequestCreatorManagerInterface;
use App\Http\Services\DealRequestService\DealRequestData;
use App\Http\Services\DealRequestService\DealRequestInterface;
use App\Http\Services\DealRequestService\DealRequestService;
use App\Http\Services\DealService\DealService;
use App\Http\Services\DealService\DealServiceInterface;
use App\Http\Services\RabbitMqService\RabbitMqService;

class DealRequestController extends Controller
{
    private DealRequestCreatorManagerInterface $service;

    public function __construct(DealRequestCreatorManagerInterface $service)
    {
        $this->service = $service;
    }

    public function create(RequestDealRequest $request): \Illuminate\Http\JsonResponse
    {
        $dealRequestData = new DealRequestData($request->get('category_id'), $request->get('price'),
            $request->get('comment'), $request->get('tag_ids'));

        $this->service->create($dealRequestData);

        return response()->json(['status' => 'ok']);
    }

    public function getActiveRequest()
    {
        return response($this->service->getActiveRequest());
    }

    public function changePriceRequest(ChangePriceRequest $request)
    {
        return response($this->service->changePrice($request->get('price')));
    }
}
