<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePriceRequest;
use App\Http\Requests\DealHistoryRequest;
use App\Http\Requests\RequestDealRequest;
use App\Http\Services\DealRequestService\DealRequestData;
use App\Http\Services\DealRequestService\DealRequestInterface;
use App\Http\Services\DealRequestService\DealRequestService;
use App\Http\Services\DealService\DealService;
use App\Http\Services\DealService\DealServiceInterface;
use App\Http\Services\RabbitMqService\RabbitMqService;

class DealController extends Controller
{
    private DealServiceInterface $dealService;

    public function __construct(DealService $dealService)
    {
        $this->dealService = $dealService;
    }

    public function getAll()
    {
        return response($this->dealService->getAll());
    }

    public function getActiveDeal()
    {
        return response($this->dealService->getActiveDeal());
    }
}
