<?php


namespace App\Http\Controllers\Web\Auth;


use App\Http\Controllers\Controller;
use App\Http\Requests\LoginWebRequest;
use App\Http\Services\AccountService\AccountService;
use App\Models\Logs;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;


/**
 * @group  Account management for Web
 *
 * APIs for managing web users
 */

class AuthController extends Controller
{
    /** @var AccountService */
    protected $service;

    public function __construct(AccountService $service)
    {
        $this->service = $service;
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param Request $request
     * @return bool
     */
    protected function login(LoginWebRequest $request)
    {
        try {
            $token = $this->service->loginWeb($request->get('login'), $request->get('password'));
        } catch (\Throwable $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 403);
        }

        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
        ])->header('Authorization', $token);
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     * @return void
     */
    public function logout(Request $request)
    {
        auth()->logout();
    }


    public function me()
    {
        return response()->json(array(
            'data' => auth()->user()
        ));
    }

    protected function register(Request $data)
    {
        return User::create([
            'name' => $data->get('name'),
            'phone' => '123112122',
            'role' => 'customer',
            'password' => bcrypt($data->get('password')),
        ]);
    }
}
