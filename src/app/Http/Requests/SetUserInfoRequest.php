<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class SetUserInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $typeEnum = User::COUNSELOR_ROLE . ',' . User::CUSTOMER_ROLE;
        return [
            'first_name' => 'string|min:2|max:20',
            'last_name' => 'string|min:2|max:20',
            'middle_name' => 'string|min:2|max:20',
            'birth_date' => 'date',
            'avatar' => 'image',
        ];
    }
}
