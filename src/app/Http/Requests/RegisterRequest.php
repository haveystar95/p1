<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $typeEnum = User::COUNSELOR_ROLE . ',' . User::CUSTOMER_ROLE;
        return [
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:13',
            'type' => "required|in:{$typeEnum}",
            'device_id' => "required",
            'push_token' => "required",
        ];
    }
}
