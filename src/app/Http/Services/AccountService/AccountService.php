<?php


namespace App\Http\Services\AccountService;


use App\Http\Services\AccountService\Exceptions\CredentialsNotValidException;
use App\Http\Services\AccountService\Exceptions\FailCreateOrUpdateUserException;
use App\Http\Services\AccountService\Exceptions\FailGetVerificationCodeException;
use App\Http\Services\AccountService\Exceptions\device_idRemoveException;
use App\Http\Services\AccountService\Exceptions\RefreshTokenException;
use App\Http\Services\AccountService\Exceptions\TypeNotMatchException;
use App\Http\Services\AccountService\Exceptions\UserNotFoundException;
use App\Http\Services\AccountService\Exceptions\VerificationCodeNotMatchException;
use App\Http\Services\AccountService\Exceptions\VerificationExpiredException;
use App\Http\Services\AccountService\Exceptions\VerificationLimitExceededException;
use App\Http\Services\AccountService\Exceptions\VerificationRequestNotValidException;
use App\Http\Services\LogService\LogService;
use App\Http\Services\SmsService\SmsService;
use App\Models\Customer;
use App\Models\Counselor;
use App\Models\Logs;
use App\Models\UserAuth;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Token;
use \Firebase\JWT\JWT;


class AccountService
{
    /** @var SmsService\SmsService */
    private $smsService;

    protected LogService $logService;

    public function __construct(SmsService $smsService, LogService $logService)
    {
        $this->logService = $logService;
        $this->smsService = $smsService;
    }

    public function create(string $phoneNumber, string $type, $deviceId, $ip, $pushToken): bool
    {
        $user = User::firstWhere(['phone' => $phoneNumber]);

        if ($user && $user instanceof User && $user->type != $type) {
            throw new TypeNotMatchException();
        } elseif (!$user) {
            $user = new User();
            if ($type == 'customer') {
                $userType = new Customer();
            } else {
                $userType = new Counselor();
            }
            $userType->save();
        } else {
            $userType = $user->user;
        }

        $verificationCode = $this->smsService->codeVerifyOnNumber($phoneNumber);

        if (!$verificationCode) {
            throw new FailGetVerificationCodeException('Failed to get verification code');
        }

        $user->phone = $phoneNumber;

        $user->type = $type;

        try {
            $resultSave = !$userType->user()->save($user);
            $this->setUserAuthInfo($user, $deviceId, $verificationCode, $pushToken);
        } catch (\Exception $exception) {
            throw new FailCreateOrUpdateUserException($exception->getMessage(), $exception->getTraceAsString());
        }
        return true;
    }

    private function getExpireVerifyDate(): string
    {
        return CarbonImmutable::now()->add(env('SMS_VERIFICATION_EXPIRE_MINUTES'), 'minutes')->toDateTimeString();
    }

    public function verifyCode(string $phoneNumber, string $verifyCode, string $deviceId): array
    {
        $userAuth = UserAuth::where(['device_id' => $deviceId])->first();

        if (!$userAuth) {
            throw new UserNotFoundException();
        }

        if ($userAuth->phone_verified_at < Carbon::now()->toDateTimeString()) {
            throw new VerificationExpiredException();
        }

        if (is_null($userAuth->verification_code) || empty($userAuth->verification_code)) {
            throw new VerificationRequestNotValidException();
        }

        if ($userAuth->fail_number_attempts >= env('NUMBER_ATTEMPTS_VERIFICATION')) {
            throw new VerificationLimitExceededException();
        }

        if ($userAuth->verification_code != $verifyCode) {
            $userAuth->fail_number_attempts++;
            $userAuth->save();
            throw new VerificationCodeNotMatchException();
        }

        $userAuth->verification_code = null;

        $userAuth->phone_verified_at = null;

        $userAuth->save();

        return $this->loginApp($userAuth, $deviceId);
    }


    public function loginApp(UserAuth $userAuth, string $deviceId): array
    {
        $user = $userAuth->user()->first();

        if (!$token = auth()->login($user)) {
            throw new CredentialsNotValidException();
        }
        $userAuth->refresh_token_check = Str::random(64);
        $userAuth->save();

        $refreshToken = $this->generateRefreshToken($deviceId, $userAuth->refresh_token_check);

        return ['token' => $token, 'refresh_token' => $refreshToken];
    }

    public function loginWeb(string $login, string $password)
    {
        if (!$token = JWTAuth::attempt(['name' => $login, 'password' => $password])) {
            throw new CredentialsNotValidException();
        }
        return $token;
    }

    public function setUserInfo(array $data)
    {
        $user = auth()->user();
        $userType = $user->user;

        try {
            if (key_exists('avatar', $data)) {
                $avatar = $data['avatar'];
                $name = $user->id . '.' . $avatar->getClientOriginalExtension();

                $link = 'storage/avatar/' . $name;
                $avatar->storeAs('public/avatar', $name);
                URL::asset($link);
                $data['link_avatar'] = 'avatar/'.$name;
            }

            $userType->fill($data)->save();
        } catch (\Throwable $exception) {
            throw new FailCreateOrUpdateUserException($exception->getMessage(), $exception->getTraceAsString());
        }
        return true;

    }

    public function setUserAuthInfo(User $user, string $deviceId, string $verificationCode, $pushToken): bool
    {
        $userAuth = UserAuth::where(['device_id' => $deviceId])->first();

        if (!$userAuth) {
            $userAuth = $user->auth()->create(['device_id' => $deviceId, 'push_token' => $pushToken]);
        }

        if ($userAuth->user_id != $user->id) {
            $userAuth->user_id = $user->id;
        }

        $userAuth->fail_number_attempts = 0;
        $userAuth->verification_code = $verificationCode;
        $userAuth->phone_verified_at = $this->getExpireVerifyDate();
        $userAuth->push_token = $pushToken;
        $userAuth->save();

        return true;
    }

    public function removedevice_idForUser($deviceId)
    {
        $user = auth()->user();

        $deviceId = UserAuth::where(['device_id' => $deviceId])->first();

        if ($deviceId && $deviceId->user_id == $user->id) {
            $deviceId->delete();
            return true;
        }

        throw new device_idRemoveException();
    }

    private function generateRefreshToken(string $deviceId, string $checkToken): string
    {
        $expire = Carbon::now();

        $expire->addDays(env('JWT_REFRESH_TTL_DAYS'));

        $payload = array(
            "iss" => "p1",
            "device_id" => $deviceId,
            "exp" => $expire->getTimestamp(),
            "check_token" => $checkToken
        );

        return JWT::encode($payload, env("JWT_SECRET"));
    }

    public function refreshToken($refreshToken): array
    {
        try {
            $payload = (array)JWT::decode($refreshToken, env("JWT_SECRET"), ['HS256']);
        } catch (\Throwable $exception) {
            throw new RefreshTokenException();
        }

        $userAuth = UserAuth::where(['device_id' => $payload['device_id']])->first();

        if ($userAuth->refresh_token_check !== $payload['check_token']) {
            throw new RefreshTokenException();
        }

        if (auth()->user()) {
            auth()->logout();
        }

        return $this->loginApp($userAuth, $payload['device_id']);
    }
}
