<?php


namespace App\Http\Services\AccountService\Exceptions;


class VerificationCodeNotMatchException extends VerificationCodeException
{
    const MESSAGE_UA = 'Код веріфікації не співпадає.';
    const MESSAGE = "The verification code does not match.";
}
