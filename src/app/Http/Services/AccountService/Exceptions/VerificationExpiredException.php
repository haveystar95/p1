<?php


namespace App\Http\Services\AccountService\Exceptions;


class VerificationExpiredException extends VerificationCodeException
{
    const MESSAGE_UA = 'Час перевірки минув.';
    const MESSAGE = "Verification time expired.";
}
