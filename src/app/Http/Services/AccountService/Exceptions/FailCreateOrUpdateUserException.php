<?php


namespace App\Http\Services\AccountService\Exceptions;

use App\Exceptions\ServerException;

class FailCreateOrUpdateUserException extends ServerException
{

}
