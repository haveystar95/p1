<?php


namespace App\Http\Services\AccountService\Exceptions;


use App\Exceptions\ClientException;
use App\Exceptions\ExceptionsCode;
use Throwable;

class CredentialsNotValidException extends ClientException
{
    const CODE = 403;
    const MESSAGE_UA = 'Помилка входу. Невірні данні для входу.';
    const MESSAGE = "Error while login. Credentials not valid.";
}
