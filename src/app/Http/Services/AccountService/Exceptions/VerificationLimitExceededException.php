<?php


namespace App\Http\Services\AccountService\Exceptions;


class VerificationLimitExceededException extends VerificationCodeException
{
    const MESSAGE_UA = 'Кількість спроб перевищена.';
    const MESSAGE = "Attempts limit exceeded.";
}
