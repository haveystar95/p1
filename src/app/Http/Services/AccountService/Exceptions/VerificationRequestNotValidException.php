<?php


namespace App\Http\Services\AccountService\Exceptions;


class VerificationRequestNotValidException extends VerificationCodeException
{
    const MESSAGE_UA = 'Запит на веріфікацію недійсний.';
    const MESSAGE = "The request is not valid.";
}
