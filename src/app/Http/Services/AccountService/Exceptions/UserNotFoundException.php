<?php


namespace App\Http\Services\AccountService\Exceptions;


use App\Exceptions\ClientException;

class UserNotFoundException extends VerificationCodeException
{
    const MESSAGE_UA = 'За цим номером користувача не знайдено.';
    const MESSAGE = "User not found by number.";
}
