<?php


namespace App\Http\Services\AccountService\Exceptions;


use App\Exceptions\ClientException;

class RefreshTokenException extends ClientException
{
    const MESSAGE_UA = 'Неможливо оновити токен. Токен не валідний';
    const MESSAGE = "Refresh token is invalid";

    const CODE = 400;

}
