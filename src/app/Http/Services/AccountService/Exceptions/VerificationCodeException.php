<?php


namespace App\Http\Services\AccountService\Exceptions;


use App\Exceptions\ClientException;

class VerificationCodeException extends ClientException
{
    const CODE = 400;
}
