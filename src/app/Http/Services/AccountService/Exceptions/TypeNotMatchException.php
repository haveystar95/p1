<?php


namespace App\Http\Services\AccountService\Exceptions;


use App\Exceptions\ClientException;
use App\Exceptions\ExceptionsCode;
use Throwable;

class TypeNotMatchException extends ClientException
{
    const CODE = 400;
    const MESSAGE_UA = 'Помилка входу. Цей номер телефону не віповідає заданій ролі.';
    const MESSAGE = "Type does not match.";
}
