<?php


namespace App\Http\Services\AccountService\Exceptions;


use App\Exceptions\ClientException;
use App\Exceptions\ExceptionsCode;
use Throwable;

class PushtokenRemoveException extends ClientException
{
    const CODE = 400;
    const MESSAGE_UA = 'Помилка виходу з системи';
    const MESSAGE = "device_id remove error.";
}
