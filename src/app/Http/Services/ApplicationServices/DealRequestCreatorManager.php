<?php


namespace App\Http\Services\ApplicationServices;


use App\Http\Services\DealInfoService\DealInfoServiceInterface;
use App\Http\Services\DealRequestService\DealRequestData;
use App\Http\Services\DealRequestService\DealRequestInterface;
use App\Http\Services\DealRequestService\DealRequestService;

class DealRequestCreatorManager implements DealRequestCreatorManagerInterface
{
    private DealRequestInterface $dealRequest;

    private DealInfoServiceInterface $dealInfoService;

    public function __construct(DealRequestInterface $dealRequest, DealInfoServiceInterface $dealInfoService)
    {
        $this->dealRequest = $dealRequest;
        $this->dealInfoService = $dealInfoService;
    }

    public function create(DealRequestData $dealRequestData): void
    {
        $dealInfo = $this->dealInfoService->create($dealRequestData);

        $dealRequest = $this->dealRequest->create($dealInfo);
    }
}
