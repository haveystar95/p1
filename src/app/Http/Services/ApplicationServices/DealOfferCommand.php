<?php


namespace App\Http\Services\ApplicationServices;


class DealOfferCommand
{
    private int $uid;

    public function __construct($uid)
    {
        $this->uid = $uid;
    }

    public function getUid(): int
    {
        return $this->uid;
    }
}
