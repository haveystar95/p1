<?php


namespace App\Http\Services\ApplicationServices;


use App\Http\Services\DealRequestService\DealRequestData;

interface DealRequestCreatorManagerInterface
{
    public function create(DealRequestData $dealRequestData): void;
}
