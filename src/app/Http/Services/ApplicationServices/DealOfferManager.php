<?php


namespace App\Http\Services\ApplicationServices;

use Kreait\Firebase\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\RawMessageFromArray;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpSender;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\Connection;

use App\Models\DealRequest;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Middleware\SendMessageMiddleware;
use Symfony\Component\Messenger\Transport\Sender\SendersLocatorInterface;
use Kreait\Laravel\Firebase\Facades\Firebase;



class DealOfferManager
{

    public function __construct(Messaging $messaging)
    {
        $this->messaging = $messaging;
    }

    public function sendOffers()
    {
//        $messageBus = new MessageBus();
        $command = new DealOfferCommand(1);
//
//        $connection = Connection::fromDsn(env('MESSENGER_TRANSPORT_DSN'));

//        $sender = new AmqpSender($connection);

//        $messageBus->dispatch($command, [
//            new AmqpStamp('DealRequest', 0),
//        ]);

        $sendersLocator = new class implements SendersLocatorInterface {
            public function getSenders(Envelope $envelope): iterable
            {
                $dsn = 'amqp://guest:guest@host.docker.internal:5672/%2f/DealRequest';

                $connection = Connection::fromDsn($dsn);

                return [
                    'async' => new AmqpSender($connection)
                ];
            }
        };

        $middleware = new SendMessageMiddleware($sendersLocator);

        $bus = new MessageBus([$middleware]);

        $bus->dispatch($command);
    }

    public function firebase()
    {
//        $message = CloudMessage::withTarget('token', 'dUawD3YFTKG_pnNYxRrIyn:APA91bE3lgBmCteqWHE58VRzkxOFzcBojFnyE8XKXsY-a1HL0qMzgdj4E6ai69viPQ9czsKy5rPDRd1leNYkQYNCujmcxPweF8e1juVXVqjVbCJUgi45Sr3K9ukX6wNOh-pnYFIHys2s')
//            ->withNotification("sads") // optional
//            ->withData([]) // optional
//        ;

//        $messaging = $factory->createMessaging();
//
        $messaging = app('firebase.messaging');
//
//        $message = CloudMessage::fromArray([
//            'token' => 'dUawD3YFTKG_pnNYxRrIyn:APA91bE3lgBmCteqWHE58VRzkxOFzcBojFnyE8XKXsY-a1HL0qMzgdj4E6ai69viPQ9czsKy5rPDRd1leNYkQYNCujmcxPweF8e1juVXVqjVbCJUgi45Sr3K9ukX6wNOh-pnYFIHys2s',
//            'notification' => [/* Notification data as array */], // optional
//            'data' => [/* data array */], // optional
//        ]);
//
//        $message->send($message);

    }

}
