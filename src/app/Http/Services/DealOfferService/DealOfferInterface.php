<?php


namespace App\Http\Services\DealOfferService;


use App\Models\DealRequest;

interface DealOfferInterface
{
    public function sendOffers(DealRequest $dealRequest);
}
