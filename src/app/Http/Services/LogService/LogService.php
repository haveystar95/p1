<?php


namespace App\Http\Services\LogService;


use App\Models\Logs;

class LogService
{

    public function create($type, $data, $stacktrace = null)
    {
        $log = Logs::create(['type' => $type, 'data' => $data, 'stacktrace' => $stacktrace]);
    }
}
