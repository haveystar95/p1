<?php

namespace App\Http\Services\DealService;

use App\Models\Category;
use App\Models\Counselor;
use App\Models\Customer;
use App\Models\Deal;
use App\Models\DealInfo;
use App\Models\Tag;

class DealEntity
{
    private Deal $deal;

    private DealInfo $info;

    private Counselor $counselor;

    private Customer $customer;

    private array $tags = [];

    private Category $category;


    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
        $this->setDealInfo();
        $this->setCounselor();
        $this->setCustomer();
        $this->setTagsInfo();
        $this->setCategory();
    }

    private function setDealInfo()
    {
        $deal = clone $this->deal;
        $this->info = $deal->dealInfo;
    }

    private function setCounselor()
    {
        $deal = clone $this->deal;
        $this->counselor = $deal->counselor->user()->first();
    }

    private function setCustomer()
    {
        $deal = clone $this->deal;
        $this->customer = $deal->customer->user()->first();
    }

    private function setTagsInfo()
    {
        $tagIds = json_decode($this->info->tag_ids, true);

        foreach ($tagIds ?? [] as $tagId) {
            $this->tags[] = Tag::find($tagId)->name;
        }
    }

    private function setCategory()
    {
        $info = clone $this->info;
        $this->category = $info->category;
    }

    /**
     * @return Deal
     */
    public function getDeal(): Deal
    {
        return $this->deal;
    }

    /**
     * @return DealInfo
     */
    public function getInfo(): DealInfo
    {
        return $this->info;
    }

    /**
     * @return Counselor
     */
    public function getCounselor(): Counselor
    {
        return $this->counselor;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }


    public function toResponse(): array
    {
        return [
            'deal' => $this->getDeal()->toArray(),
            'info' => $this->getInfo()->toArray(),
            'customer' => $this->getCustomer(),
            'counselor' => $this->getCounselor(),
            'category' => $this->getCategory(),
            'tags' => $this->getTags()
        ];
    }
}
