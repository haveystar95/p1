<?php


namespace App\Http\Services\DealService;

use App\Http\Services\DealRequestService\DealOfferInterface;
use App\Http\Services\DealService\Exceptions\ActiveDealNotFoundException;
use App\Http\Services\LogService\LogService;
use App\Models\Deal;
use App\Models\DealRequest;
use App\Models\User;
use Kreait\Firebase\Messaging;

class DealService implements DealServiceInterface
{
    private LogService $logService;
    private Messaging $messaging;

    public function __construct(LogService $logService, Messaging $messaging)
    {
        $this->logService = $logService;
        $this->messaging = $messaging;
    }

    public function getAll(): array
    {
        return [];
    }

    public function getActiveDeal(): array
    {
        $activeDeal = Deal::where(['customer_id' => auth()->id(), 'status' => Deal::STATUS_ACTIVE])->first();

        if (!($activeDeal instanceof Deal)) {
            throw new ActiveDealNotFoundException();
        }

        $dealEntity = new DealEntity($activeDeal);

        return $dealEntity->toResponse();
    }

    public function getHistoryDeals(?int $fromId = 1): array
    {
        $fromId = $fromId ?? 1;
        $historyDeals = Deal::where(['customer_id' => auth()->id(), 'status' => Deal::STATUS_DONE])->where('id', '>=',
            $fromId)->limit(10)->get();

        $result = [];

        foreach ($historyDeals as $historyDeal) {
            $result[] = (new DealEntity($historyDeal))->toResponse();
        }

        return $result;
    }


    public function send()
    {
        $message = Messaging\CloudMessage::fromArray([
            'token' => 'fWzocsC9yUd1qNZvutfQWb:APA91bEsqximsPe9o0y4_CiqiyHMcg34Y9TuMDR_8mI2oUrRqapEdYIXmVtn19gWR2EcQlg2OktDHqTRxluS_J3lWANd4Q_zx8KgunRNpXB3NUa1mJC-XJO57Hv1GK9i4PMdzck_apXC',
            'notification' => [
                "title" => 'Жека пидо',
                "body" => '',
                "priority" => "high",
                "sound" => "default",
                "tag" => "SOME_UNIQUE_TAG",
                "click_action" => "FLUTTER_NOTIFICATION_CLICK"
            ],
            'data' => ['1' => '2e'], // optional
        ]);

        $this->messaging->send($message);
    }


}
