<?php


namespace App\Http\Services\DealService\Exceptions;


use App\Exceptions\ClientException;

class ActiveDealNotFoundException extends ClientException
{
    const CODE = 404;

    const MESSAGE_UA = 'Не знайдено активних заказів.';
    const MESSAGE = "Active deal not found.";
}
