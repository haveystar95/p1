<?php


namespace App\Http\Services\DealService;


use App\Models\DealRequest;

interface DealServiceInterface
{
    public function getAll(): array;

    public function getActiveDeal(): array;

    public function getHistoryDeals(?int $fromId): array;

    public function send();

}
