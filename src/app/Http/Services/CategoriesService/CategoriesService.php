<?php


namespace App\Http\Services\CategoriesService;


use App\Http\Services\LogService\LogService;
use App\Models\Category;

class CategoriesService implements CategoriesServiceInterface
{
    private LogService $logService;

    public function __construct(LogService $logService)
    {
        $this->logService = $logService;
    }

    public function getAll()
    {
        return $categories = Category::with('tags')->get();
    }
}
