<?php


namespace App\Http\Services\DealRequestService\Exceptions;


use App\Exceptions\ClientException;

class ActiveDealRequestNotFoundException extends ClientException
{
    const CODE = 400;

    const MESSAGE_UA = 'Активний запит не знайдено.';
    const MESSAGE = "Active request not found.";
}
