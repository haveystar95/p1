<?php


namespace App\Http\Services\DealRequestService\Exceptions;


use App\Exceptions\ClientException;

class DealRequestAlreadyExistException extends ClientException
{
    const CODE = 400;

    const MESSAGE_UA = 'Кристувач вже має активний запит.';
    const MESSAGE = "This user already has request.";
}
