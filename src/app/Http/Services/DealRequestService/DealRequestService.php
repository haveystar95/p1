<?php


namespace App\Http\Services\DealRequestService;


use App\Http\Services\DealRequestService\Exceptions\ActiveDealRequestNotFoundException;
use App\Http\Services\DealRequestService\Exceptions\DealRequestAlreadyExistException;
use App\Http\Services\LogService\LogService;
use App\Models\DealInfo;
use App\Models\DealRequest;
use App\Repositories\DealRequestRepositoryInterface;

class DealRequestService implements DealRequestInterface
{
    private LogService $logService;

    private DealRequestRepositoryInterface $dealRequestRepository;

    public function __construct(DealRequestRepositoryInterface $dealRequestRepository, LogService $logService)
    {
        $this->logService = $logService;
        $this->dealRequestRepository = $dealRequestRepository;
    }

    public function create(DealInfo $dealInfo): DealRequest
    {
        return $this->dealRequestRepository->createByDealInfo($dealInfo);
    }

    private function checkAlreadyExistRequest(DealRequestData $dealRequestData): void
    {
        if (DealRequest::where(['customer_id' => $dealRequestData->getCustomerId()])->exists()) {
            throw new DealRequestAlreadyExistException();
        }
    }

    public function getActiveRequest(): array
    {
        $activeDealRequest = $this->dealRequestRepository->getActiveRequestByCustomerId();

        if (is_null($activeDealRequest)) {
            throw new ActiveDealRequestNotFoundException();
        }

        $dealRequest = new DealRequestEntity($activeDealRequest);

        return $dealRequest->toResponse();
    }

    public function changePrice(int $price): bool
    {
        $activeDealRequest = DealRequest::where([
            'customer_id' => auth()->id(),
            'status' => DealRequest::STATUS_PENDING_REQUEST
        ])->first();

        $activeDealRequest->price = $price;

        $activeDealRequest->save();

    }

}
