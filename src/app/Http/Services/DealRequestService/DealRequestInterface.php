<?php


namespace App\Http\Services\DealRequestService;

use App\Models\DealInfo;
use App\Models\DealRequest;

interface DealRequestInterface
{
    public function create(DealInfo $dealInfo): DealRequest;

    public function changePrice(int $price): bool;

    public function getActiveRequest(): array;
}
