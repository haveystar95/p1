<?php


namespace App\Http\Services\DealRequestService;


use App\Models\Category;
use App\Models\Counselor;
use App\Models\Customer;
use App\Models\Deal;
use App\Models\DealInfo;
use App\Models\DealRequest;
use App\Models\Tag;

class DealRequestEntity
{
    private DealRequest $dealRequest;

    private DealInfo $info;

    private Counselor $counselor;

    private Customer $customer;

    private array $tags = [];

    private Category $category;


    public function __construct(DealRequest $dealRequest)
    {
        $this->dealRequest = $dealRequest;

        $this->setDealRequestInfo();
        $this->setCustomer();
        $this->setTagsInfo();
        $this->setCategory();
    }

    private function setDealRequestInfo()
    {
        $dealRequest = clone $this->dealRequest;
        $this->info = $dealRequest->dealInfo;
    }

    private function setCustomer()
    {
        $deal = clone $this->dealRequest;
        $this->customer = $deal->customer->user()->first();
    }

    private function setTagsInfo()
    {
        $tagIds = json_decode($this->info->tag_ids, true);

        foreach ($tagIds ?? [] as $tagId) {
            $this->tags[] = Tag::find($tagId)->name;
        }
    }

    private function setCategory()
    {
        $info = clone $this->info;
        $this->category = $info->category;
    }

    public function getDealRequest(): DealRequest
    {
        return $this->dealRequest;
    }

    /**
     * @return DealInfo
     */
    public function getInfo(): DealInfo
    {
        return $this->info;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }


    public function toResponse(): array
    {
        return [
            'dealRequest' => $this->getDealRequest()->toArray(),
            'info' => $this->getInfo()->toArray(),
            'customer' => $this->getCustomer(),
            'category' => $this->getCategory(),
            'tags' => $this->getTags()
        ];
    }
}
