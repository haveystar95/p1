<?php


namespace App\Http\Services\DealRequestService;


/**
 * Class DealRequestData
 * @package App\Http\Services\DealRequestService
 */
class DealRequestData
{
    /**
     * @var int
     */
    private int $customerId;

    /**
     * @var int
     */
    private int $categoryId;

    /**
     * @var int
     */
    private int $price;

    /**
     * @var string
     */
    private string $comment;

    /**
     * @var array
     */
    private array $tagIds;


    /**
     * DealRequestData constructor.
     * @param int $categoryId
     * @param int $price
     * @param string $comment
     * @param array $tagIds
     */
    public function __construct(int $categoryId, int $price, string $comment, array $tagIds)
    {
        $this->categoryId = $categoryId;
        $this->price = $price;
        $this->comment = $comment;
        $this->tagIds = $tagIds;

        $this->setCustomerIdAsCurrentUser();
    }

    private function setCustomerIdAsCurrentUser(): void
    {
        $this->customerId = auth()->id();
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getTagIds(): string
    {
        return json_encode($this->tagIds);
    }

    /**
     * @param array $tagIds
     */
    public function setTagIds(array $tagIds)
    {
        $this->tagIds = $tagIds;
    }

}
