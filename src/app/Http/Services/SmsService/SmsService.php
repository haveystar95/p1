<?php


namespace App\Http\Services\SmsService;


use App\Http\Services\LogService\LogService;
use App\Http\Services\SmsService\Exceptions\FailSendRequestSmsException;
use App\Models\Logs;
use http\Exception\RuntimeException;
use Illuminate\Support\Facades\Http;
use phpDocumentor\Reflection\Types\Mixed_;

class SmsService
{
    private const HOST = 'https://smsc.ua/sys/send.php?';

    private $url;

    /** @var LogService */
    private $loggerService;

    public function __construct(string $login, string $password, LogService $loggerService)
    {
        $this->loggerService = $loggerService;
        $this->url = self::HOST . "login={$login}&psw={$password}";
    }

    public function codeVerifyOnNumber(string $number)
    {
        $code = $this->generateCode();
        try {
            $response = Http::get($this->url . "&phones={$number}&mes={$code}");
        } catch (\Exception $exception) {
            throw new FailSendRequestSmsException($exception->getMessage(), $exception->getTraceAsString());
        }

        $json = $response->json() ?? null;

        if ($response->status() == 200 && $json && key_exists('error', $json)) {
            throw new FailSendRequestSmsException($json['error'], $response->body());
        }

        return $code;
    }

    private function generateCode(): string
    {
        return rand(pow(10, 4 - 1), pow(10, 4) - 1);
    }
}
