<?php


namespace App\Http\Services\SmsService\Exceptions;


use App\Exceptions\ServerException;

class FailSendRequestSmsException extends ServerException
{

}
