<?php
namespace App\Http\Services\DealInfoService;

use App\Http\Services\DealRequestService\DealRequestData;
use App\Models\DealInfo;
use App\Repositories\DealInfoRepositoryInterface;

class DealInfoService implements DealInfoServiceInterface
{
    private DealInfoRepositoryInterface $dealInfoRepository;

    public function __construct(DealInfoRepositoryInterface $dealInfoRepository)
    {
        $this->dealInfoRepository = $dealInfoRepository;
    }

    public function create(DealRequestData $data): DealInfo
    {
        return $this->dealInfoRepository->saveByRequestData($data);
    }
}
