<?php

namespace App\Http\Services\DealInfoService;

use App\Http\Services\DealRequestService\DealRequestData;
use App\Models\DealInfo;

interface DealInfoServiceInterface
{
    public function create(DealRequestData $data): DealInfo;
}
