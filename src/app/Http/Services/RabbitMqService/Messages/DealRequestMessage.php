<?php


namespace App\Http\Services\RabbitMqService\Messages;


use PhpAmqpLib\Message\AMQPMessage;

class DealRequestMessage extends AbstractMessage implements MessageInterface
{
    public int $categoryId;

    public int $customerId;

    public string $tagIds;

    public string $comment;

    public int $price;

    public string $createdAt;

    public array $additionalParams = [];


    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     */
    public function setCustomerId(int $customerId): void
    {
        $this->customerId = $customerId;
    }

    /**
     * @return array
     */
    public function getTagIds(): array
    {
        return $this->tagIds;
    }

    /**
     * @param string $tagIds
     */
    public function setTagIds(string $tagIds): void
    {
        $this->tagIds = $tagIds;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return array
     */
    public function getAdditionalParams(): array
    {
        return $this->additionalParams;
    }

    /**
     * @param array $additionalParams
     */
    public function setAdditionalParams(array $additionalParams): void
    {
        $this->additionalParams = $additionalParams;
    }

}
