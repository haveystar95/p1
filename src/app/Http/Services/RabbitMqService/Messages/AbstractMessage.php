<?php


namespace App\Http\Services\RabbitMqService\Messages;


use http\Exception\RuntimeException;
use PhpAmqpLib\Message\AMQPMessage;

abstract class AbstractMessage
{
    public static function fromAmqpMessage(AMQPMessage $message): self
    {
        $object = new static();

        try {
            foreach (json_decode($message->getBody(), true) as $property => $value) {
                $object->$property = $value;
            }
        } catch (\Throwable $exception) {
            throw new RuntimeException('Not valid message');
        }

        return $object;
    }


}
