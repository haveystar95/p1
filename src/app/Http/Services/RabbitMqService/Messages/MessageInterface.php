<?php


namespace App\Http\Services\RabbitMqService\Messages;


use PhpAmqpLib\Message\AMQPMessage;

interface MessageInterface
{
    public static function fromAmqpMessage(AMQPMessage $message);
}
