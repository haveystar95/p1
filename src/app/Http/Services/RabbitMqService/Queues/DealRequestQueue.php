<?php


namespace App\Http\Services\RabbitMqService\Queues;


class DealRequestQueue implements QueueInterface
{
    public const NAME = 'DealRequest';

    public static function getName(): string
    {
        return self::NAME;
    }
}
