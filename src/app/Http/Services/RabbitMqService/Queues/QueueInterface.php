<?php


namespace App\Http\Services\RabbitMqService\Queues;


interface QueueInterface
{
    public static function getName(): string;

}
