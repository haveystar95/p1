<?php


namespace App\Http\Services\RabbitMqService;


use App\Http\Services\RabbitMqService\Messages\MessageInterface;

interface RabbitMqInterface
{

    public function updateSettings(): void;

    public function publish(MessageInterface $message, $queue);

    public function consume(string $queueName, $callbackClass, $params = []);
}
