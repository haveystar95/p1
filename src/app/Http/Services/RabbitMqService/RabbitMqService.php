<?php


namespace App\Http\Services\RabbitMqService;


use App\Http\Services\RabbitMqService\Messages\MessageInterface;
use App\Http\Services\RabbitMqService\Queues\DealRequestQueue;
use App\Http\Services\RabbitMqService\Queues\QueueInterface;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

class RabbitMqService implements RabbitMqInterface
{
    private const INCLUDE_QUEUE = [
        DealRequestQueue::class
    ];

    private const EXCHANGES = [
        'main' => 'mainDirect'
    ];

    private AMQPStreamConnection $connection;

    private AMQPChannel $channel;

    public function __construct(AMQPStreamConnection $connection)
    {
        $this->connection = $connection;
        $this->channel = $this->connection->channel();
    }

    public function updateSettings(): void
    {
        $mainArguments = new AMQPTable();
        $mainArguments->set('x-max-priority', 5);

        foreach (self::EXCHANGES as $exchange) {
            $this->channel->exchange_declare($exchange, 'direct', false, true, false);
        }

        /** @var $queue QueueInterface */
        foreach (self::INCLUDE_QUEUE as $queue) {
            $this->channel->queue_declare($queue::getName(), false, true, false, false,
                false, $mainArguments);
            $this->channel->queue_bind($queue::getName(), self::EXCHANGES['main'],
                $queue::getName());
        }
    }

    public function publish(MessageInterface $message, $queue)
    {
        $message = new AMQPMessage(json_encode($message), [
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
        ]);

        $this->channel->basic_publish($message, self::EXCHANGES['main'], $queue::getName());

    }

    public function __destruct()
    {
        if (isset($this->channel)) {
            try {
                $this->channel->close();
            } catch (\Exception $e) {
                // ignore on shutdown
            }
        }

        if (isset($this->connection) && $this->connection->isConnected()) {
            try {
                $this->connection->close();
            } catch (\Exception $e) {
                // ignore on shutdown
            }
        }
    }

    public function consume(string $queueName, $callbackClass, $params = [])
    {
        $startTime = time();

        $workingTime = 0;

        $subscriberTimeout = 100;

        $this->channel->basic_qos(null, 1, null);

        $this->channel->basic_consume($queueName, '', false, false, false, false, [$callbackClass, 'process']);

        while (count($this->channel->callbacks)) {
            if ($workingTime > $subscriberTimeout) {
                print_r('[SUBSCRIBER_DIED_WORK] : ' . get_class($callbackClass) . ' working time: ' . $workingTime . PHP_EOL);
                break;
            }
            $workingTime = time() - $startTime;

            try {
                $this->channel->wait(null, false, 100);
            } catch (AMQPTimeoutException $e) {
                print_r("[SUBSCRIBER_DIED_TIMEOUT] " . $e->getMessage() . PHP_EOL);
                die();
            } catch (Exception $e) {
                print_r("[SUBSCRIBER_DIED] " . $e->getMessage() . PHP_EOL);
                die();
            }
        }
    }


}
